/**
 *
 * Input
 * soThu1
 * soThu2
 * soThu3
 * tongChan
 * tongLe
 *
 * Todo
 * dùng if else và xét giá trị: soThu1, soThu2, soThu3 không là chuỗi rỗng và phải khác nhau nếu không yêu cầu kiểm tra lại thông tin
 * sét từng số nếu %2 ==0 thì cộng 1 vào tongChan nếu không cộng 1 vào tong Le
 *
 *
 * Output
 * bao nhiêu số chẵn và bào nhiêu số lẻ
 *
 */

let demSo = function () {
  let soThu1 = document.getElementById(`sothu1`).value * 1;
  let soThu2 = document.getElementById(`sothu2`).value * 1;
  let soThu3 = document.getElementById(`sothu3`).value * 1;

  let tongChan = 0;
  let tongLe = 0;

  if (
    soThu1 != "" &&
    soThu2 != "" &&
    soThu3 != "" &&
    soThu1 != soThu2 &&
    soThu2 != soThu3 &&
    soThu3 != soThu1
  ) {
    if (soThu1 % 2 == 0) {
      tongChan++;
    } else {
      tongLe++;
    }
    if (soThu2 % 2 == 0) {
      tongChan++;
    } else {
      tongLe++;
    }
    if (soThu3 % 2 == 0) {
      tongChan++;
    } else {
      tongLe++;
    }
  } else {
    return (document.getElementById(
      `result-ex3`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  document.getElementById(
    `result-ex3`
  ).innerHTML = `Có ${tongChan} số chẵn và ${tongLe} số lẻ`;

  $("#form-3").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};

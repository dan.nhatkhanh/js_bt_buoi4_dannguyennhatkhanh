/**
 *
 * Input
 * Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)
 *
 *
 * Todo
 * dùng switch case cho Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)
 *
 * Output
 * Chào hỏi từng người khi được chọn
 *
 */

const chaoThanhVien = function () {
  const thanhVien = document.getElementById(`thanhvien`).value;

  switch (thanhVien) {
    case "C":
      document.getElementById(
        `result-ex2`
      ).innerHTML = `Vui lòng chọn thành viên!`;
      break;
    case "B":
      document.getElementById(`result-ex2`).innerHTML = `Chào Bố`;
      break;
    case "M":
      document.getElementById(`result-ex2`).innerHTML = `Chào Mẹ`;
      break;
    case "A":
      document.getElementById(`result-ex2`).innerHTML = `Chào Anh hai`;
      break;
    case "E":
      document.getElementById(`result-ex2`).innerHTML = `Chào Em út`;
      break;

    default:
      break;
  }

  $("#form-2").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};

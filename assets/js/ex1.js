/**
 *
 * Input
 * soNguyen1
 * soNguyen2
 * soNguyen3
 *
 * Todo
 * dùng if else kết hợp với esle if
 *
 * xét giá trị: soNguyen1, soNguyen2, soNguyen3 không là chuỗi rỗng và phải khác nhau nếu không yêu cầu kiểm tra lại thông tin
 *
 * xét các trường hợp xảy ra:
 * ví dụ: soNguyen1, soNguyen2, soNguyen3 là a,b,c thì có các trường hợp sau:
 * b<a<c
 * c<a<b
 * a<b<c
 * c<b<a
 * a<c<b
 * b<c<a
 *
 * Output
 * soNguyen1 , soNguyen2, soNguyen3 theo thứ tự tăng dần
 *
 */

const sapXep = function () {
  const soNguyen1 = +document.getElementById(`songuyen1`).value;
  const soNguyen2 = +document.getElementById(`songuyen2`).value;
  const soNguyen3 = +document.getElementById(`songuyen3`).value;

  if (
    soNguyen1 != "" &&
    soNguyen2 != "" &&
    soNguyen3 != "" &&
    soNguyen1 != soNguyen2 &&
    soNguyen2 != soNguyen3 &&
    soNguyen3 != soNguyen1
  ) {
    if (soNguyen2 < soNguyen1 && soNguyen1 < soNguyen3) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen2} < ${soNguyen1} < ${soNguyen3}`;
    } else if (soNguyen3 < soNguyen1 && soNguyen1 < soNguyen2) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen3} < ${soNguyen1} < ${soNguyen2}`;
    } else if (soNguyen1 < soNguyen2 && soNguyen2 < soNguyen3) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen1} < ${soNguyen2} < ${soNguyen3}`;
    } else if (soNguyen3 < soNguyen2 && soNguyen2 < soNguyen1) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen3} < ${soNguyen2} < ${soNguyen1}`;
    } else if (soNguyen1 < soNguyen3 && soNguyen3 < soNguyen2) {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen1} < ${soNguyen3} < ${soNguyen2}`;
    } else {
      document.getElementById(
        `result-ex1`
      ).innerHTML = `${soNguyen2} < ${soNguyen3} < ${soNguyen1}`;
    }
  } else {
    return (document.getElementById(
      `result-ex1`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  $("#form-1").submit(function (e) {
    e.preventDefault();
  });
};

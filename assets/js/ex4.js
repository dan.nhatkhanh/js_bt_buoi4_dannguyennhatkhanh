/**
 *
 * Input
 * canh1
 * canh2
 * canh3
 *
 * Todo
 * dùng if else kết hợp với esle if
 *
 * xét giá trị: canh1, canh2, canh3 không là chuỗi rỗng và phải khác nhau nếu không yêu cầu kiểm tra lại thông tin
 *
 * xét điều kiện cho từng loại tam giác
 * điều kiện 1: canh1 == canh2 && canh2 == canh3 -> Tam giác đều
 * điều kiện 2: canh1 == canh 2 || canh2 == canh3 || canh3 == canh1 -> Tam giác cân
 * điều kiện 3:
 * canh1 == Math.sqrt(canh2 * canh2 + canh3 * canh3) ||
 * canh2 == Math.sqrt(canh1 * canh1 + canh3 * canh3) ||
 * canh3 == Math.sqrt(canh2 * canh2 + canh1 * canh1)
 * -> Tam giác vuông
 *
 * Còn lại là Tam giác khác
 *
 *
 * Output
 * loại tam giác
 *
 */

const doanTamGiac = function () {
  const canh1 = document.getElementById(`canh1`).value * 1;
  const canh2 = document.getElementById(`canh2`).value * 1;
  const canh3 = document.getElementById(`canh3`).value * 1;

  if (canh1 != "" && canh2 != "" && canh3 != "") {
    if (canh1 == canh2 && canh2 == canh3) {
      document.getElementById(`result-ex4`).innerHTML = `Hình Tam giác đều`;
    } else if (canh1 == canh2 || canh2 == canh3 || canh3 == canh1) {
      document.getElementById(`result-ex4`).innerHTML = `Hình Tam giác cân`;
    } else if (
      canh1 == Math.sqrt(canh2 * canh2 + canh3 * canh3) ||
      canh2 == Math.sqrt(canh1 * canh1 + canh3 * canh3) ||
      canh3 == Math.sqrt(canh2 * canh2 + canh1 * canh1)
    ) {
      document.getElementById(`result-ex4`).innerHTML = `Hình Tam giác vuông`;
    } else {
      document.getElementById(`result-ex4`).innerHTML = `Hình Tam giác khác`;
    }
  } else {
    return (document.getElementById(
      `result-ex4`
    ).innerHTML = `Vui lòng kiểm tra lại thông tin!`);
  }

  $("#form-4").submit(function (e) {
    e.preventDefault(); // <==stop page refresh==>
  });
};
